'use strict'

const { test, trait } = use('Test/Suite')('endpoints response');
const Hero = use('App/Models/Hero');

trait('Test/ApiClient');

test('Create a valid hero', async ({ client, assert }) => {

  const response = await client.post('heroes/registrer').send({
    name: "Barry Allen",
    alias: "Flash",
    age: 25,
    DI: 123456
  }).end();


  response.assertStatus(200);
  response.assertJSONSubset({
    message: "Hero created successfully",
    hero: {
      name: "Barry Allen",
      alias: "Flash",
      age: 25,
      DI: 123456
    }
  });

  const hero = await Hero.find(1);

  assert.equal(hero.name, "Barry Allen");
  assert.equal(hero.alias, "Flash");
  assert.equal(hero.age, 25);
  assert.equal(hero.DI, 123456);

  await hero.delete();
});


test('Create a exits hero', async ({ client, assert }) => {

  const hero = await  Hero.create({name:"Barry Allen",alias:"Flash",age:25,DI:123456});

  const response = await client.post('heroes/registrer').send({
    name: "Barry Allen",
    alias: "Flash",
    age: 25,
    DI: 123456
  }).end();

  response.assertStatus(400);
  response.assertJSONSubset({
    message: "Error creating hero"
  });

  await hero.delete();
});
